# SGC Analyser tool

This tool generates an RPGM-based GUI, used by PP's tax team to compute the Super Charge Guarantee.

## Usage

- Generate the PGM file
  - Clone the repo
  - Install RCode and RPGM from [here](https://www.pgm-solutions.com/rpgm/download)
  - Get a licensed account from our RPGM admin (As of April 2022, ask Dilan) and log in
  - Open the project contained in the repo. It's important to use the included project because it includes versioning information. This is an RPGM oddity.
  - In RCode, click PGM > Export as PGM, and save it to your choice of location. You can save it into the repo if you like, since .pgm files are ignored
- Upload the PGM file to PP's RPGM server
  - Log in [here](https://rpgm.pitcher.com.au) with your RPGM account details. These should be the same as earlier, but if not, talk to our RPGM admin
  - Click "Add program" button on the dashboard, and drag-and-drop your PGM file into the box
  - Click the play button on your program. If an instance doesn't open automatically, click "Connect" for your newly created instance on the dashboard
- Running the calculation
  - On the Payroll Data tab, click Browse and drag the payroll, super and payroll mapping files in
  - In each of Payroll Data, Super Data and Payroll Mapping, in Browse select the appropriate file (don't click the eye or the bin, just click the text) and **click Upload**
  - On the Calculation tab, wait for the "No Upload" section to change to "Data Check Passed", and then click Calculate SGC. If it's taking more than a few seconds, check each of Payroll Data, Super Data and Payroll mapping - you may have forgotten to click Upload for one of them
- Exporting files
  - Use the Export Files tab to access various summary tables, and download them to your choice of local folder
