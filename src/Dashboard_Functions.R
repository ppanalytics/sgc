#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-# 
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-# 
#-#                                                                         #-# 
#-#                           DASHBOARD FUNCTIONS                           #-# 
#-#                                                                         #-# 
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-# 
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-# 


###########################-DASHBOARD TAB-##################################

generategraph1 <- function(){

    results_quarter <- results_quarter %>% ungroup()

    xData <- vector(
        mode = "list", 
        length = results_quarter %>% 
            #ungroup() %>% 
            select(Quarter) %>%
            distinct() %>%
            nrow()  
        )
    
    for (i in 1:length(xData)){
        xData[[i]] <- (
            results_quarter %>% 
            #ungroup() %>% 
            select(Quarter) %>%
            distinct() %>%
            arrange(Quarter)
        )[i,1]$Quarter
    }

    yData <- vector(mode = "list", length = length(xData))

    for (i in 1:length(yData)){
        yData[[i]] <- (results_quarter %>% 
            #ungroup() %>% 
            filter(Quarter == xData[[i]]) %>% 
            #select(SF) %>% 
            select(Shortfall_Net) %>% 
            #summarise(SG_total = sum(as.numeric(SF))) %>%
            summarise(SG_total = sum(as.numeric(Shortfall_Net)), na.rm = TRUE) %>%
            select(SG_total)
            )$SG_total
    }

    for (i in 2:length(yData)){
        yData[[i]] <- yData[[i]] + yData[[i-1]]
        yData[[i-1]] <- round(yData[[i-1]], 2)
    }

    trace1 = list(
        x = xData,
        y = yData,
        type = 'scatter',
        mode = 'lines+markers',
        line = list(
            color = 'rgba(0,118,118,1)'
        ),
        hovertemplate = "Total Shortfall as of %{x}: %{y:$,.0f}",
        name = ''
    )

    data = list(trace1)

    layout = list(
        # title = 'SG Timeline',
        xaxis = list(
            # title = 'Pay Period',
            showgrid = FALSE,
            showline = FALSE
        ),
        yaxis = list(
            # title = 'SG Underpayment ($)',
            showgrid = FALSE,
            showline = FALSE
        ),
        height = 450,
        width = 550,
        margin = list(
            l = 50,
            r = 50, 
            b = 80,
            t = 50,
            pad = 2
        ),
        hovermode = 'closest',
        # showlegend = FALSE,
        paper_bgcolor = 'rgba(0,0,0,0)',
        plot_bgcolor = 'rgba(0,0,0,0)'
    )

    return(list(traces=data, layout=layout))
    #     data = list(
    #     values = list(
    #         round(length(which(results_employee$SGC_Net > 1 )),0), 
    #         round(as.numeric(super_guarantee_summary[1,3])-length(which(results_employee$SGC_Net > 1 )),0)
    #     ),
    #     labels = list('Affected Employees', 'Unaffected Employees'),
    #     # textinfo = 'label+percent+value',
    #     textinfo = 'value',
    #     marker = list(
    #         colors = list(
    #             # 'rgba(244, 123, 32, 1)',
    #             'rgba(0,181,158,1)',
    #             'rgba(0, 118, 118, 1)'
    #         )
    #     ),
    #     # textposition = 'outside',
    #     insidetextorientation = "horizontal",
    #     hoverinfo = 'label+percent',
    #     hole = .4,
    #     type = 'pie'
    # )

    # layout = list(
    #     # title = 'Summary of SGC by Employees',
    #     autosize = FALSE,
    #     annotations = list(
    #         font = list(
    #             size = 15,
    #             color = 'rgba(0, 118, 118, 1)'
    #         ),
    #         showarrow = FALSE,
    #         text = 'Employees',
    #         x = 0.5,
    #         y = 0.5,
    #         xref = 'x',
    #         yref = 'y'
    #     ),
    #     height = 300,
    #     width = 300,
    #     margin = list(
    #         l = 20,
    #         r = 50, 
    #         b = 100,
    #         t = 100,
    #         pad = 10
    #     ),
    #     showlegend = TRUE,
    #     paper_bgcolor = 'rgba(0,0,0,0)',
    #     plot_bgcolor = 'rgba(0,0,0,0)'
    # )

    # return(
    #     list(
    #         traces = data, 
    #         layout = layout,
    #         list(
    #             displaylogo = FALSE
    #             # responsive = TRUE
    #         )
    #     )
    # )

    # data = list(
    #     values = list(
    #         super_guarantee_summary[11,"Calcs"], 
    #         round(sum(results_quarter$Super_Actual)-super_guarantee_summary[11,"Calcs"],0)
    #     ),
    #     labels = list('Underpayment', 'On-time Payments'),
    #     # textinfo = 'label+percent+value',
    #     textinfo = 'value',
    #     marker = list(
    #         colors = list(
    #             # 'rgba(244, 123, 32, 1)',
    #             'rgba(0,181,158,1)',
    #             'rgba(0, 118, 118, 1)'
    #         )
    #     ),
    #     # textposition = 'outside',
    #     insidetextorientation = "horizontal",
    #     hoverinfo = 'label+percent',
    #     hole = .4,
    #     type = 'pie'
    # )

    # layout = list(
    #     # title = 'Summary of SGC by Amount',
    #     autosize = FALSE,
    #     annotations = list(
    #         font = list(size=15),
    #         showarrow = FALSE,
    #         text = 'SGC',
    #         x = 0.5,
    #         y = 0.5,
    #         xref = 'x',
    #         yref = 'y'
    #     ),
    #     height = 200,
    #     width = 200,
    #     margin = list(
    #         l = 20,
    #         r = 50, 
    #         b = 50,
    #         t = 20,
    #         pad = 5
    #     ),
    #     showlegend = TRUE,
    #     paper_bgcolor = 'rgba(0,0,0,0)',
    #     plot_bgcolor = 'rgba(0,0,0,0)'
    # )

    # config = list(
    #     scrollZoom = TRUE,
    #     displaylogo = FALSE
    # )

    # return(list(traces=data, layout=layout, xaxis = list(rangemode = 'tozero'), config=config))
}


generategraph2 <- function(){


    waterfall_table <- data.frame(
    "y_trace0" = c(
                0,
                super_guarantee_summary[4,"Calcs"],
                super_guarantee_summary[4,"Calcs"],
                super_guarantee_summary[6,"Calcs"],
                super_guarantee_summary[6,"Calcs"] + super_guarantee_summary[7,"Calcs"],
                super_guarantee_summary[11,"Calcs"],
                0),
    "y_runningtotal" = c(
                super_guarantee_summary[2,"Calcs"],
                0,
                0,
                0,
                0,
                0,
                super_guarantee_summary[11,"Calcs"]),
    "y_increases" = c(
                0,
                0,
                super_guarantee_summary[5,"Calcs"],
                super_guarantee_summary[7,"Calcs"],
                super_guarantee_summary[8,"Calcs"],
                0,
                0),
    "y_decreases" = c(
                0,
                super_guarantee_summary[3,"Calcs"],
                0,
                0,
                0,
                super_guarantee_summary[10,"Calcs"],
                0)
    )
    

    runningtotal_text <- list()
    increases_text <- list()
    decreases_text <- list()

    for (i in 1:nrow(waterfall_table)){
        runningtotal_text[[i]] <- ifelse( waterfall_table[i,"y_runningtotal"] >=1000,
        paste('$',formatC(round(waterfall_table[i,"y_runningtotal"]/1000,2), big.mark=',', format = 'd'),"K"),
            ifelse(
                waterfall_table[i,"y_runningtotal"] >0,
                paste('$',round(waterfall_table[i,"y_runningtotal"],0)),
                ""
            )
        )
        increases_text[[i]] <- ifelse( waterfall_table[i,"y_increases"] >=1000,
        paste('$',formatC(round(waterfall_table[i,"y_increases"]/1000,2), big.mark=',', format = 'd'),"K"),
            ifelse(
                waterfall_table[i,"y_increases"] >0,
                paste('$',round(waterfall_table[i,"y_increases"],0)),
                ""
            )
        )
        decreases_text[[i]] <- ifelse( waterfall_table[i,"y_decreases"] >=1000,
        paste('$',formatC(round(waterfall_table[i,"y_decreases"]/1000,2), big.mark=',', format = 'd'),"K"),
            ifelse(
                waterfall_table[i,"y_decreases"] >0,
                paste('$',round(waterfall_table[i,"y_decreases"],0)),
                ""
            )
        )
    }
  
   xData = c('Superannuation<br>Underpayment', 'Less<br>Carry Forward<br>Payments Offset','Salary and Wages<br>Component of<br>SG Shortfall',
            'Accrued<br>Interest', 'Admin<br>Fees', 'Less<br>Late Payment<br>Offset','Superannuation<br>Guarantee<br>Charge'
  )
   
  trace0 = list(
    x = xData,
    y = waterfall_table$y_trace0,
    marker = list(
      color = 'rgba(1,1,1,0.0)'
    ),
    type = 'bar',
    hoverinfo = 'none',
    connector = list(
        mode="between", 
        line=list(
            width=4, 
            color="rgb(0, 0, 0)", 
            dash="solid"
        )
    )
  )
 
  runningtotal = list(
    x = xData,
    y = waterfall_table$y_runningtotal,
    type = 'bar',
    hoverinfo = 'none',
    text = runningtotal_text, 
    #text = list('$257.3K', '', '$46.4K', '', '$47.7K', '', '', '$71.0K', '', '$49.6K'),
    textposition = 'auto',
    marker = list(
      color = 'rgba(0,185,242,0.7)',
      line = list(
        color = 'rgba(0,185,242,1)',
        width = 2
      )
    ),
    connector = list(
        mode="between", 
        line=list(
            width=4, 
            color="rgb(0, 0, 0)", 
            dash="solid"
        )
    )
  )
 
  increases = list(
    x = xData,
    y = waterfall_table$y_increases,
    type = 'bar',
    hoverinfo = 'none',
    textposition = 'auto',
    text = increases_text,
    #text = list('', '', '', '$+1.3K', '', '$+23.1K', '$+140', '', '', ''),
    marker = list(
      color = 'rgba(0,118,118,0.7)',
      line = list(
        color = 'rgba(0,118,118,1)',
        width = 2
      )
    ),
    connector = list(
        mode="between", 
        line=list(
            width=4, 
            color="rgb(0, 0, 0)", 
            dash="solid"
        )
    )
  )
 
  decreases = list(
    x = xData,
    y = waterfall_table$y_decreases,
    type = 'bar',
    hoverinfo = 'text', 
    text = decreases_text,
    #text = list('', '$-211.9K', '', '', '', '', '', '', '$-21.4K', ''),
    textposition = 'auto',
    marker = list(
      color = 'rgba(244,123,32,0.7)',
      line = list(
        color = 'rgba(244,123,32,1)',
        width = 2
      )
    ),
    connector = list(
        mode="between", 
        line=list(
            width=4, 
            color="rgb(0, 0, 0)", 
            dash="solid"
        )
    )
  )
 
  data = list(trace0, runningtotal, increases, decreases)
 
  layout = list(
    # title = 'Quarterly Level Results Summary',
    barmode = 'stack',
    paper_bgcolor = 'rgba(0,0,0,0)',
    plot_bgcolor = 'rgba(0,0,0,0)',
    width = 1200,
    height = 500,
    showlegend = FALSE,
    margin = list(
            l = 50,
            r = 50, 
            b = 80,
            t = 50,
            pad = 2
        )
  )
 
  return(list(traces=data, layout=layout))

}


generategraph3 <- function(){

    results_quarter <- results_quarter %>% ungroup()

    xData <- vector(
        mode = "list", 
        length = results_quarter %>% 
            #ungroup() %>% 
            select(Quarter) %>%
            distinct() %>%
            nrow()  
        )
    
    for (i in 1:length(xData)){
        xData[[i]] <- (
            results_quarter %>% 
            #ungroup() %>% 
            select(Quarter) %>%
            distinct() %>%
            arrange(Quarter)
        )[i,1]$Quarter
    }

    yData <- vector(mode = "list", length = length(xData))

    for (i in 1:length(yData)){
        yData[[i]] <- (results_quarter %>% 
            #ungroup() %>% 
            filter(Quarter == xData[[i]]) %>% 
            #select(SF) %>% 
            select(Shortfall_Net) %>% 
            #summarise(SG_total = sum(as.numeric(SF))) %>%
            summarise(SG_total = sum(as.numeric(Shortfall_Net), na.rm = TRUE)) %>%
            select(SG_total)
            )$SG_total
    }

    # for (i in 2:length(yData)){
    #     yData[[i]] <- yData[[i]] + yData[[i-1]]
    #     yData[[i-1]] <- round(yData[[i-1]], 2)
    # }

    trace1 = list(
        x = xData,
        y = yData,
        type = 'bar',
        # mode = 'lines+markers',
        marker = list(
            color = 'rgba(0,118,118,1)'
        ),
        hovertemplate = "Total Shortfall in %{x}: %{y:$,.0f}",
        name = ''
    )
    
    # xData2 <- vector(
    #     mode = "list", 
    #     length = results_quarter_wo_carry %>% 
    #         ungroup() %>% 
    #         select(Quarter) %>%
    #         distinct() %>%
    #         nrow()  
    #     )
    
    # for (i in 1:length(xData2)){
    #     xData2[[i]] <- (
    #         results_quarter_wo_carry %>% 
    #         ungroup() %>% 
    #         select(Quarter) %>%
    #         distinct() %>%
    #         arrange(Quarter)
    #     )[i,1]$Quarter
    # }

    # yData2 <- vector(mode = "list", length = length(xData2))

    # for (i in 1:length(yData2)){
    #     yData2[[i]] <- (results_quarter_wo_carry %>% 
    #         ungroup() %>% 
    #         filter(Quarter == xData2[[i]]) %>% 
    #         select(Total_SGC_Shortfall, Late_Payment_Offset) %>% 
    #         summarise(SG_total = sum(as.numeric(Total_SGC_Shortfall-Late_Payment_Offset))) %>%
    #         select(SG_total)
    #         )$SG_total
    # }

    # for (i in 2:length(yData2)){
    #     yData2[[i]] <- yData2[[i]] + yData2[[i-1]]
    #     yData2[[i-1]] <- round(yData2[[i-1]], 2)
    # }

    # trace2 = list(
    #     x = xData2,
    #     y = yData2,
    #     type = 'scatter',
    #     mode = 'lines+markers',
    #     line = list(
    #         # color = 'rgba(244, 123, 32, 1)'
    #         color = 'rgba(0,181,158,1)'
    #     ),
    #     hovertemplate = "<b>Without carry forward</b><br><br>Total SGC as of %{x}: %{y:$,.0f}<extra></extra>",
    #     name = 'Without<br>carry<br>forward'
    # )

    data = list(trace1)

    layout = list(
        # title = 'SG Timeline',
        xaxis = list(
            # title = 'Pay Period',
            showgrid = FALSE,
            showline = FALSE
        ),
        yaxis = list(
            # title = 'SG Underpayment ($)',
            showgrid = FALSE,
            showline = FALSE
        ),
        height = 450,
        width = 550,
        margin = list(
            l = 50,
            r = 50, 
            b = 70,
            t = 30,
            pad = 2
        ),
        hovermode = 'closest',
        # showlegend = FALSE,
        paper_bgcolor = 'rgba(0,0,0,0)',
        plot_bgcolor = 'rgba(0,0,0,0)'
    )

    return(list(traces=data, layout=layout))
}

generategraph4 <- function(){

    results_quarter <- results_quarter %>% ungroup() 

    xData <- vector(
        mode = "list", 
        length = results_quarter %>% 
                #ungroup() %>%
                distinct(Employee) %>% 
                nrow()  
        )

    yData <- vector(mode = "list", length = length(xData))
    textData <- vector(mode = "list", length = length(xData))
    
    for (i in 1:length(xData)){
        xData[[i]] <- (
            results_quarter %>% 
            #ungroup() %>%
            group_by(Employee) %>% 
            arrange(Employee) %>%
            summarise(OTE_total=sum(OTE, na.rm = TRUE))
        )$OTE_total[i]

        yData[[i]] <- (
            results_quarter %>% 
            #ungroup() %>%
            group_by(Employee) %>% 
            arrange(Employee) %>%
            summarise(SGC_charge=round(sum(Total_SGC_Shortfall-Late_Payment_Offset, na.rm = TRUE),0))
        )$SGC_charge[i]

        textData[[i]] <- (
            results_quarter %>% 
            #ungroup() %>%
            group_by(Employee) %>% 
            arrange(Employee) %>%
            summarise(OTE_total=sum(OTE, na.rm = TRUE))
        )$Employee[i]
    }

    
    yData_under <- vector(mode = "list", length = length(yData))
    yData_notunder <- vector(mode = "list", length = length(yData))
    xData_under <- vector(mode = "list", length = length(xData))
    xData_notunder <- vector(mode = "list", length = length(xData))
    textData_under <- vector(mode = "list", length = length(textData))
    textData_notunder <- vector(mode = "list", length = length(textData))

    for (i in 1:length(yData_under)){
        if (yData[[i]] > 0){
            yData_under[[i]] <- yData[[i]]
            xData_under[[i]] <- xData[[i]]
            textData_under[[i]] <- textData[[i]]
        } else {
            yData_notunder[[i]] <- yData[[i]]
            xData_notunder[[i]] <- xData[[i]]
            textData_notunder[[i]] <- textData[[i]]
        }
    } 

    yData_under = yData_under[-which(sapply(yData_under, is.null))]
    yData_notunder = yData_notunder[-which(sapply(yData_notunder, is.null))]
    xData_under = xData_under[-which(sapply(xData_under, is.null))]
    xData_notunder = xData_notunder[-which(sapply(xData_notunder, is.null))]
    textData_under = textData_under[-which(sapply(textData_under, is.null))]
    textData_notunder = textData_notunder[-which(sapply(textData_notunder, is.null))]

    trace1 = list(
        # x = xData_notunder,
        x = yData_notunder,
        # mode = 'markers',
        type = 'histogram',
        opacity = 0.7,
        name = 'Unaffected<br>Employees',      
        # text = textData_notunder,
        # hoverinfo = 'text+x',
        # hovertemplate = "<b>Employee:</b> %{text}<br><br><b>Superannuation Guarantee Charge</b>: %{x:$,.0f}<extra></extra>",
        marker = list(
            # size = 8,
            color = 'rgba(0,118,118,1)'
        )
    )

    trace2 = list(
        # x = xData_under,
        x = yData_under,
        # mode = 'markers',
        type = 'histogram',
        opacity = 0.8,
        name = 'Affected<br>Employees',      
        # text = textData_under,
        # hoverinfo = 'text+x',
        # hovertemplate = "<b>Employee:</b> %{text}<br><br><b>Superannuation Guarantee Charge</b>: %{x:$,.0f}<extra></extra>",
        marker = list(
            # size = 8,
            color = 'rgba(0,181,158,1)'
        )
    )
    
    data = list(trace2)

    layout = list(
        # title = 'Ordinary Time Earnings vs SG Underpayment',
        barmode = 'overlay',
        bargap = 0.05,
        xaxis = list(
            # title = 'Ordinary Time Earnings ($)',
            showgrid = FALSE,
            showline = FALSE
        ),
        yaxis = list(
            # title = 'Superannuation Guarantee Charge ($)',
            showgrid = FALSE,
            showline = FALSE
        ),
        # hovermode = 'closest',
        # hoverlabel = list(bgcolor = 'rgba(0,118,118,1)'),
        height = 450,
        width = 550,
        margin = list(
            l = 50,
            r = 50, 
            b = 50,
            t = 30,
            pad = 2
        ),
        showlegend = TRUE,
        paper_bgcolor = 'rgba(0,0,0,0)',
        plot_bgcolor = 'rgba(0,0,0,0)'
    )

    return(list(traces=data, layout=layout))
    
}

updateDashboardVisuals <- function(){
    
    gui.setValue("this", "MyGraph1", generategraph1())
    gui.setValue("this", "MyGraph2", generategraph2())
    gui.setValue("this", "MyGraph3", generategraph3())
    gui.setValue("this", "MyGraph4", generategraph4())
}


