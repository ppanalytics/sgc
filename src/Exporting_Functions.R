#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-# 
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-# 
#-#                                                                         #-# 
#-#                           DASHBOARD FUNCTIONS                           #-# 
#-#                                                                         #-# 
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-# 
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-# 


##############################-OUTPUT TAB-#####################################


download <- function(Download_Type){

    if(checkDirectory()){
        # Download summary tables
        if(Download_Type == "Summary Tables"){
            writeSummaryTables()
            gui.setValue("this", "Folder_Check", "<div class = 'success-wrapper'> Summary Tables Written </div>")
        }
        # Download SGC statements
        if(Download_Type == "SGC Statements"){
            writeSGCStatements()
            gui.setValue("this", "Folder_Check", "<div class = 'success-wrapper'> Summary Tables Written </div>")
        }
    }
}

checkDirectory <- function(){
    
    # check that there is a path and it is valid
    folder <- as.character(Download_Path)
    folder <- gsub("\\\\","/", folder)

    if(dir.exists(folder)){
        assign("Output_Folder", folder, envir = .GlobalEnv)
        return(TRUE)
    }
    else{
        # Display that there is an error in the folder path chosen
        gui.setValue("this", "Folder_Check", "<div class='error-wrapper'>Error with Output Folder Selected</div>")
        return(FALSE)
    }

}

downloadProgress <- function(progress_type, amount) {
    gui.setValue("this", progress_type, amount)
}

loadDetails <- function(Employee_Details_Path){
    # load the file
    Employee_Details <- readInFile(Employee_Details_Path)
    #Employee_Details <- Employee_Details %>% mutate(Employee = as.character(Employee))
    # make sure Date of Birth is a Date.
    DOB_Check <- as.Date(Employee_Details$DOB,format = "%d/%m/%Y")

    if(sum(is.na(DOB_Check))==0){
        Employee_Details <- Employee_Details %>%
            mutate(DOB = as.Date(DOB,format = "%d/%m/%Y"))
    }else{
        Employee_Details <- Employee_Details %>%
            mutate(DOB = as.Date(DOB,format = "%m/%d/%Y"))
    }
    assign("Employee_Details", Employee_Details, envir = .GlobalEnv) 

    # state that the employee details have been entered
    assign("Employee_Details_Available", TRUE, envir = .GlobalEnv) 

    

}

loadEmployerDetails <- function(Employer_Details_Path){
    # load the file
    Employer_Details <- readInFile(Employer_Details_Path)

    Employer_Details[is.na(Employer_Details)] <- ""

    assign("Employer_Details", Employer_Details, envir = .GlobalEnv) 

    # state that the employee details have been entered
    assign("Employer_Details_Available", TRUE, envir = .GlobalEnv) 

    

}

writeSummaryTables <- function(){
    
    # # Employee Quarterly Table Table
    # path <- paste(Output_Folder, "/", "Employee Quarterly View", ".csv", sep="")
    # write.csv(employees_quarter_view, path, row.names=FALSE)

    # # Quarterly Table 
    # path <- paste(Output_Folder, "/", "Total View by Quarter", ".csv", sep="")
    # write.csv(value_added_summary, path, row.names=FALSE)

    # # Results Quarter
    # path <- paste(Output_Folder, "/", "Quarterly Results", ".csv", sep="")
    # write.csv(results_quarter, path, row.names=FALSE)

    # # Results Employee
    # path <- paste(Output_Folder, "/", "Employee Results", ".csv", sep="")
    # write.csv(results_employee, path, row.names=FALSE)

    path <- paste(Output_Folder, "/", "Summary Tables", ".xlsx", sep="")

    wb <- createWorkbook()
    # Employee Quarterly Table Table
    addWorksheet(wb, "Employee Quarterly View")
    writeData(wb, "Employee Quarterly View", employees_quarter_view)
    downloadProgress("Download_Summary_Progress",20)
    # Quarterly Table 
    addWorksheet(wb, "Total View by Quarter")
    writeData(wb, "Total View by Quarter", value_added_summary)
    downloadProgress("Download_Summary_Progress",40)
    # Results Quarter
    addWorksheet(wb, "Quarterly Results")
    writeData(wb, "Quarterly Results", results_quarter)
    downloadProgress("Download_Summary_Progress",60)
    # Results Employee
    addWorksheet(wb, "Employee Results")
    writeData(wb, "Employee Results", results_employee)
    downloadProgress("Download_Summary_Progress",80)
    
    saveWorkbook(wb, file = path, overwrite = TRUE)
    downloadProgress("Download_Summary_Progress",100)


    # Select the workbook
    # wb_open_path <- "W:/Data & Analytics/02. Internal/04 SGC tool/Statements/Summary_Statement_Template.xlsx"
    # xlsx.open(wb_open_path)
    # xlsx.selectWorksheet("Employee Quarterly View");
    # for (i in 1:nrow(employees_quarter_view)){

    # }

}

writeSGCStatements <- function(){

    dates <- results_quarter %>% distinct(Quarter) %>% mutate(Year = substr(Quarter,1,4))
    years <-  unique(dates$Year)
    digits <- 0
    uploaded_amount <- 0
    uploading_amount <- length(unique(dates$Quarter)) + 1
    threshold <- 1

    #xlsx.open("W:/Data & Analytics/02. Internal/04 SGC tool/Statements/SGC_statement.xlsx")
    
    for (year in years){
        date_range <- dates %>% filter(Year == year)
        qtrs_selected <- unique(date_range$Quarter) 
        
        for (quarter_i in qtrs_selected){

            year_end <-  substr(as.character(year),3,4)

            qtr_end <- substr(quarter_i,6,6)
            qtr_year <- year
            qtr_name <- paste("MAR")

            if (qtr_end == 2){
                qtr_name <- paste("JUN")
            } else if (qtr_end == 3){
                qtr_name <- paste("SEP")
            } else if(qtr_end == 4){
                qtr_name <- paste("DEC")
            }
            
            amnesty <-  TRUE
            if( year == 2018 & qtr_end >= 2){
                    amnesty <-  FALSE
            } else if( year > 2018){
                amnesty <-  FALSE
            }

            if(amnesty == TRUE){
                wb_open_path <- paste("\\\\ppapps4\\data\\Data & Analytics\\SGC\\Statements v2\\CLIENT NAME - SGC Amnesty Statement - Qtr ",qtr_name,year_end ,".xlsx", sep= "")
                wb_close_path <- paste(Output_Folder,"/","CLIENT NAME - SGC Amnesty Statement - Qtr ",qtr_name ,year_end,".xlsx", sep= "")
                xlsx.open(wb_open_path)
            }else{
                wb_open_path <- paste("\\\\ppapps4\\data\\Data & Analytics\\SGC\\Statements v2\\CLIENT NAME - SGC Amnesty Statement - Qtr ",qtr_name,year_end ,".xlsx", sep= "")
                wb_close_path <- paste(Output_Folder,"/","CLIENT NAME - SGC Statement - Qtr ",qtr_name ,year_end,".xlsx", sep= "")
                xlsx.open(wb_open_path)
            }

            #temp_results <- results_quarter %>% filter(Quarter==quarter_i) %>% filter(SGC_Final > threshold)
            temp_results <- results_quarter %>% filter(Quarter==quarter_i) %>% filter(Shortfall_Net > threshold)
            
            assign("temp_results", temp_results, envir = .GlobalEnv)
            if (Employee_Details_Available){
                temp_results <- temp_results %>% mutate(Employee = as.character(Employee))
                temp_results <- temp_results %>% left_join(Employee_Details)
            }

            xlsx.selectWorksheet("Employee Details");
            
            count <- 1

            if(length(temp_results) > 0 ){
                for (Employee in temp_results$Employee){
                    #xlsx.setCell(paste("I",5+count, sep = ""), temp_results[[count,"Employee"]]);
                    xlsx.setCell(paste("E",4+count, sep = ""), temp_results[[count,"Shortfall_Net"]]);
                    xlsx.setCell(paste("G",4+count, sep = ""), temp_results[[count,"Late_Payment_Offset"]]);
                    #xlsx.setCell(paste("H",5+count, sep = ""), temp_results[[count,"Interests"]]);
                    if(Employee_Details_Available){
                        xlsx.setCell(paste("A",4+count, sep = ""), temp_results[[count,"TFN"]]);
                        xlsx.setCell(paste("B",4+count, sep = ""), temp_results[[count,"Full_Name"]]);
                        xlsx.setCell(paste("C",4+count, sep = ""), as.character(temp_results[[count,"DOB"]]));
                        xlsx.setCell(paste("D",4+count, sep = ""), temp_results[[count,"Address"]]);
                    }

                    count <- count + 1
                }
            }
            uploaded_amount <- uploaded_amount + 1
            downloadProgress("Download_Statement_Progress",round((uploaded_amount/uploading_amount)*100,0))

            if (Employer_Details_Available){
                xlsx.selectWorksheet("Employer Details");
                xlsx.setCell("F10", Employer_Details[[1,"TFN"]]);
                xlsx.setCell("F12", Employer_Details[[1,"ABN"]]);
                xlsx.setCell("F14", Employer_Details[[1,"Legal_Name"]]);
                xlsx.setCell("F16", Employer_Details[[1,"Trading_Name"]]);
                xlsx.setCell("F18", Employer_Details[[1,"Street_Address_1"]]);
                xlsx.setCell("F20", Employer_Details[[1,"Street_Address_2"]]);
                xlsx.setCell("F22", Employer_Details[[1,"Street_Address_3"]]);
                xlsx.setCell("F24", Employer_Details[[1,"Postal_Address_1"]]);
                xlsx.setCell("F26", Employer_Details[[1,"Postal_Address_2"]]);
                xlsx.setCell("F28", Employer_Details[[1,"Postal_Address_3"]]);
                xlsx.setCell("F30", Employer_Details[[1,"Contact"]]);
                xlsx.setCell("F32", Employer_Details[[1,"Phone"]]);
                xlsx.setCell("F34", Employer_Details[[1,"Mobile"]]);
                xlsx.setCell("F36", Employer_Details[[1,"E_Mail"]]);
                #xlsx.setCell("F38", interest_date);
                Client_Name <- Employer_Details[[1,"Entity_Name"]]

                if(amnesty == TRUE){
                    wb_close_path <- paste(Output_Folder,"/",Client_Name," - SGC Amnesty Statement - Qtr ",qtr_name ,year_end,".xlsx", sep= "")
                }else{
                    wb_close_path <- paste(Output_Folder,"/",Client_Name," - SGC Statement - Qtr ",qtr_name ,year_end,".xlsx", sep= "")

                }
                
            }
            xlsx.saveAs(wb_close_path);

            
        }

        
    }
    downloadProgress("Download_Statement_Progress",100)

}

# writeSGCStatements <- function(){

#     dates <- results_quarter %>% distinct(Quarter) %>% mutate(Year = substr(Quarter,1,4))
#     years <- unique(dates$Year) 
#     digits <- 0
#     uploaded_amount <- 0
#     uploading_amount <- length(unique(dates$Quarter)) + 1
#     threshold <- 1

#     #xlsx.open("W:/Data & Analytics/02. Internal/04 SGC tool/Statements/SGC_statement.xlsx")
    
#     for (year in years){
#         date_range <- dates %>% filter(Year == year)
#         qtrs_selected <- unique(date_range$Quarter) 
#         wb_open_path <- paste("W:/Data & Analytics/02. Internal/04 SGC tool/Statements/SGC_statement_",year ,".xlsx", sep= "")
#         wb_close_path <- paste(Output_Folder,"/","SGC_statement_",year ,"_Output",".xlsx", sep= "")
#         xlsx.open(wb_open_path)
        
#         for (quarter_i in qtrs_selected){

#             #temp_results <- results_quarter %>% filter(Quarter==quarter_i) %>% filter(SGC_Final > threshold)
#             temp_results <- results_quarter %>% filter(Quarter==quarter_i) %>% filter(Shortfall_Net > threshold)
            
#             assign("temp_results", temp_results, envir = .GlobalEnv)
#             if (Employee_Details_Available){
#                 temp_results <- temp_results %>% left_join(Employee_Details)
#             }

#             qtr_end <- substr(quarter_i,6,6)
#             qtr_year <- year
#             qtr_name <- paste("Mar"," ",substr(qtr_year,3,4), sep = "")

#             if (qtr_end == 2){
#                 qtr_name <- paste("Jun"," ",substr(quarter_i,3,4),sep = "")
#             } else if (qtr_end == 3){
#                 qtr_name <- paste("Sep"," ",substr(quarter_i,3,4),sep = "")
#             } else if(qtr_end == 4){
#                 qtr_name <- paste("Dec"," ",substr(quarter_i,3,4),sep = "")
#             }

#             quarter_summary_sheet <- paste(qtr_name," Summary",sep="")

#             xlsx.selectWorksheet(quarter_summary_sheet);

#             employee_count <- nrow(temp_results)
#             Total_SG_Shortfall <- sum(temp_results$Shortfall_Net)
#             Nominal_Interest_Component <- sum(temp_results$Interests)
#             Admin_Component <- sum(temp_results$Admin_fee)
#             Sub_Total <- Total_SG_Shortfall + Nominal_Interest_Component + Admin_Component
#             Late_Payment_Offset <- sum(temp_results$Late_Payment_Offset)
#             Total_SGC_Payable <- sum(temp_results$SGC_Final)
            
#             xlsx.setCell("F4", employee_count);
#             xlsx.setCell("F7", round(Total_SG_Shortfall,2));
#             xlsx.setCell("F11", round(Nominal_Interest_Component,2));
#             xlsx.setCell("F13", round(Admin_Component,2));
#             xlsx.setCell("F15", round(Sub_Total,2));
#             xlsx.setCell("F18", round(Late_Payment_Offset,2));
#             xlsx.setCell("F21", round(Total_SGC_Payable,2));

#             quarter_employee_sheet <- paste(qtr_name," Employee",sep="")

#             xlsx.selectWorksheet(quarter_employee_sheet);
            
#             count <- 1
#             for (Employee in temp_results$Employee){
#                 xlsx.setCell(paste("I",4+count, sep = ""), temp_results[[count,"Employee"]]);
#                 xlsx.setCell(paste("E",4+count, sep = ""), temp_results[[count,"Shortfall_Net"]]);
#                 xlsx.setCell(paste("G",4+count, sep = ""), temp_results[[count,"Late_Payment_Offset"]]);
#                 xlsx.setCell(paste("H",4+count, sep = ""), temp_results[[count,"Interests"]]);
#                 if(Employee_Details_Available){
#                     xlsx.setCell(paste("A",4+count, sep = ""), temp_results[[count,"TFN"]]);
#                     xlsx.setCell(paste("B",4+count, sep = ""), temp_results[[count,"Full_Name"]]);
#                     xlsx.setCell(paste("C",4+count, sep = ""), as.character(temp_results[[count,"DOB"]]));
#                     xlsx.setCell(paste("D",4+count, sep = ""), temp_results[[count,"Address"]]);
#                 }

#                 count <- count + 1
#             }
#             uploaded_amount <- uploaded_amount + 1
#             downloadProgress("Download_Statement_Progress",round((uploaded_amount/uploading_amount)*100,0))
#         }

#         xlsx.saveAs(wb_close_path);
#     }
#     downloadProgress("Download_Statement_Progress",100)

# }


# # Download SGC form

# dl_sgc <- function(){
  
#     # Sys.setenv('JAVA_HOME' = 'C:/Program Files/Java/jre1.8.0_251/')
#     # a_form <- loadWorkbook('W:/Data & Analytics/02. Internal/04 SGC tool/ATO_Template_v3.xlsx', create=TRUE)
#     # setStyleAction(a_form,XLC$"STYLE_ACTION.NONE")

#     # writeWorksheet(a_form, 5, "Totals", startRow=3, startCol=6, header=TRUE)
#     # # writeWorksheet(a_form, 5, "Totals", startRow=2, startCol=6, header=TRUE)
#     # # writeWorksheet(a_form, 5, "Totals", startRow=4, startCol=6, header=TRUE)
#     # # writeWorksheet(a_form, 5, "Totals", startRow=5, startCol=6, header=TRUE)
    
#     # saveWorkbook(a_form, file=rpgm.outputFile('trial1.xlsx'))
#     qtrs <- unique(results_quarter$Quarter) 

#     wb <- read_excel('W:/Data & Analytics/02. Internal/04 SGC tool/ATO_Template.xlsx', sheet='Totals')
#     wb[is.na(wb)]<-""

#     # assign("sgc_summary_form", wb, envir = .GlobalEnv)
#     # gui.setValue("this","SGC_Summary_Form", sgc_summary_form)
#     digits <- 0

#     for (i in qtrs){

#         a_form <- wb

#         temp_results <- results_quarter %>% filter(Quarter==i)

#         temp_employee <- temp_results %>% 
#             mutate(SGC_Net=Total_SGC_Shortfall-Late_Payment_Offset)

#         a_form[1,12] <- substr(i,1,4)
#         qtr_end <- substr(i,6,6)

#         if (qtr_end == 1){
#             a_form[1,10] <- 31
#             a_form[1,11] <- '03'
#         } else if (qtr_end == 2){
#             a_form[1,10] <- 30
#             a_form[1,11] <- '06'
#         } else if (qtr_end == 3){
#             a_form[1,10] <- 31
#             a_form[1,11] <- '09'
#         } else {
#             a_form[1,10] <- 31
#             a_form[1,11] <- '12'
#         }

#         a_form[6,5] <- round(sum(temp_results$SF),digits)
#         a_form[8,5] <- 0

#         a_form[10,5] <- round(sum(temp_results$Interests),digits)
#         a_form[12,5] <- round(sum(temp_results$Admin_fee),digits)
#         a_form[14,5] <- round(sum(temp_results$Total_SGC_Shortfall),digits)
#         a_form[17,5] <- round(sum(temp_results$Late_Payment_Offset),digits)
#         a_form[20,5] <- round(sum(temp_results$Total_SGC_Shortfall) - sum(temp_results$Late_Payment_Offset),digits)

#         a_form[3,5] <- round(length(which(temp_employee$SGC_Net > 1 )),0)

#         filename = sprintf('SGC_Form_%s.csv', i)

#         write.csv(a_form, file=rpgm.outputFile(filename), row.names=FALSE)
#     }

# }


# # Download Employee details form

# dl_employee_details <- function(){

#     qtrs <- unique(results_quarter$Quarter) 

#     wb <- read_excel('W:/Data & Analytics/02. Internal/04 SGC tool/ATO_Template.xlsx', sheet='Employee details')
#     wb[is.na(wb)]<-""

#     digits <- 2

#     for (i in qtrs){

#         a_form <- wb

#         temp_results <- results_quarter %>% filter(Quarter==i)

#         a_form[1,12] <- substr(i,1,4)
#         qtr_end <- substr(i,6,6)

#         if (qtr_end == 1){
#             a_form[1,10] <- 31
#             a_form[1,11] <- '03'
#         } else if (qtr_end == 2){
#             a_form[1,10] <- 30
#             a_form[1,11] <- '06'
#         } else if (qtr_end == 3){
#             a_form[1,10] <- 31
#             a_form[1,11] <- '09'
#         } else {
#             a_form[1,10] <- 31
#             a_form[1,11] <- '12'
#         }

#         employee_list <- unique(temp_results$Employee)

#         for (j in 1:nrow(employee_list)){

#             if ((temp_results %>% filter(Employee==employee_list[j]))$Shortfall_Net > 0){

#                 a_form[j+3,2] <- employee_list[j]

#                 a_form[j+3,5] <- (temp_results %>% filter(Employee==employee_list[j]))$Shortfall_Net
#                 a_form[j+3,6] <- 0
#                 a_form[j+3,7] <- (temp_results %>% filter(Employee==employee_list[j]))$Late_Payment_Offset
#                 a_form[j+3,8] <- (temp_results %>% filter(Employee==employee_list[j]))$Interests

#             }
#         }

#         filename = sprintf('Employee_details_%s.csv', i)

#         write.csv(a_form, file=rpgm.outputFile(filename), row.names=FALSE)
#     }

# }

